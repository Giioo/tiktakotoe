package com.example.myapplication


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_tic_tac_toe.*

class TicTacToeActivity : AppCompatActivity() {

    private var isPlayerOne: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tic_tac_toe)
        init()
    }

    private fun init() {
        Button00.setOnClickListener {
            changeButton(Button00)
        }
        Button01.setOnClickListener {
            changeButton(Button01)
        }
        Button02.setOnClickListener {
            changeButton(Button02)
        }
        Button10.setOnClickListener {
            changeButton(Button10)
        }
        Button11.setOnClickListener {
            changeButton(Button11)
        }
        Button12.setOnClickListener {
            changeButton(Button12)
        }
        Button20.setOnClickListener {
            changeButton(Button20)
        }
        Button21.setOnClickListener {
            changeButton(Button21)
        }
        Button22.setOnClickListener {
            changeButton(Button22)
        }
        playAgainButton.setOnClickListener {
            buttonContainer.visibility = View.VISIBLE
            resultTextView.text = ""
            isPlayerOne = true
            Button00.isClickable = true
            Button00.text = ""
            Button01.isClickable = true
            Button01.text = ""
            Button02.isClickable = true
            Button02.text = ""
            Button10.isClickable = true
            Button10.text = ""
            Button11.isClickable = true
            Button11.text = ""
            Button12.isClickable = true
            Button12.text = ""
            Button20.isClickable = true
            Button20.text = ""
            Button21.isClickable = true
            Button21.text = ""
            Button22.isClickable = true
            Button22.text = ""
        }
    }

    private fun changeButton(Button: Button) {
        d("click", "Button")
        if (isPlayerOne) {
            Button.text = "X"
        } else {
            Button.text = "0"
        }
        Button.isClickable = false
        isPlayerOne = !isPlayerOne
        checkWinner()
    }

    private fun checkWinner() {
        if (Button00.text.isNotEmpty() && Button00.text.toString() == Button01.text.toString() && Button00.text.toString() == Button02.text.toString()) {
            resultTextView.text = "Winner is ${Button00.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button10.text.isNotEmpty() && Button10.text.toString() == Button11.text.toString() && Button10.text.toString() == Button12.text.toString()) {
            resultTextView.text = "Winner is ${Button10.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button20.text.isNotEmpty() && Button20.text.toString() == Button21.text.toString() && Button20.text.toString() == Button22.text.toString()) {
            resultTextView.text = "Winner is ${Button20.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button01.text.isNotEmpty() && Button01.text.toString() == Button11.text.toString() && Button01.text.toString() == Button21.text.toString()) {
            resultTextView.text = "Winner is ${Button01.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button02.text.isNotEmpty() && Button02.text.toString() == Button12.text.toString() && Button12.text.toString() == Button22.text.toString()) {
            resultTextView.text = "Winner is ${Button02.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button00.text.isNotEmpty() && Button00.text.toString() == Button11.text.toString() && Button11.text.toString() == Button22.text.toString()) {
            resultTextView.text = "Winner is ${Button00.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button00.text.isNotEmpty() && Button00.text.toString() == Button10.text.toString() && Button10.text.toString() == Button20.text.toString()) {
            resultTextView.text = "Winner is ${Button00.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button20.text.isNotEmpty() && Button20.text.toString() == Button11.text.toString() && Button20.text.toString() == Button02.text.toString()) {
            resultTextView.text = "Winner is ${Button20.text}"
            buttonContainer.visibility = View.GONE
        } else if (Button00.text.isNotEmpty() && Button01.text.isNotEmpty() && Button02.text.isNotEmpty() && Button10.text.isNotEmpty() && Button20.text.isNotEmpty()
            && Button11.text.isNotEmpty() && Button12.text.isNotEmpty() && Button22.text.isNotEmpty() && Button21.text.isNotEmpty()){
            resultTextView.text = "Game is Draw"
            buttonContainer.visibility = View.GONE
        }
    }
}